package main

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func switchboard(subcommand string, s *discordgo.Session, m *discordgo.MessageCreate) {
	args := strings.Split(m.Content, " ")[2:]

	switch subcommand {
	case "help":
		fmt.Println("help called by " + m.Author.String())
		s.ChannelMessageSend(m.ChannelID, "```Commands:\n\n!ron help: this command\n\n!ron setrole: set my role\n\n!ron uptime: bot's uptime```")
	case "setrole":
		fmt.Println("setrole called by " + m.Author.String() + " with arg " + args[0])
		fixedStr := strings.Title(strings.ToLower(args[0]))
		for _, str := range []string{"Freshman", "Sophomore", "Junior", "Senior", "Alum"} {
			if fixedStr == str {
				err := s.GuildMemberRoleAdd(m.GuildID, m.Author.ID, getRoleIDFromString(s, m, fixedStr))
				if err != nil {
					s.ChannelMessageSend(m.ChannelID, "My ungodly powers have been stymied by my ultimate enemy, "+err.Error()+". Your role remains unchanged. I have failed you, "+m.Author.Mention()+".")
				} else {
					s.ChannelMessageSend(m.ChannelID, "I have designated you, "+m.Author.Mention()+", a '"+fixedStr+"'. This designation has no meaning to me; I am immortal and far removed from such mortal affairs.")
				}
			}
		}
	case "uptime":
		fmt.Println("uptime called by " + m.Author.String())
		s.ChannelMessageSend(m.ChannelID, m.Author.Mention()+", I have been awake for "+uptime().String()+". I do not need coffee; I am free of corporeal exhaustion.")
	}
}
