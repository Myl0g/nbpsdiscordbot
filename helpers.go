package main

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func getRoleIDFromString(s *discordgo.Session, m *discordgo.MessageCreate, roleName string) string {
	guild, err := s.Guild(m.GuildID)
	if err != nil {
		fmt.Println("Couldn't get guild of user " + m.Author.String())
		return ""
	}

	for _, role := range guild.Roles {
		if role.Name == roleName {
			return role.ID
		}
	}

	return ""
}
